import pytest
import unittest
from models.links import *
from models.locators import *
from playwright.sync_api import Page


class VerifyPicturesSuit(unittest.TestCase):
    @pytest.fixture(autouse=True)
    def setup(self, page: Page):
        self.page = page

    def test_verify_pictures(self):
        self.page.goto(allrealty)
        # Click [placeholder="Лучший ЖК"]
        self.page.click("[placeholder=\"Лучший ЖК\"]")
        # Fill [placeholder="Лучший ЖК"]
        for filters in filter_search:
            self.page.fill("[placeholder=\"Лучший ЖК\"]", filters)
            # Press Enter
            self.page.press("[placeholder=\"Лучший ЖК\"]", "Enter")
            self.page.click("text=Найти")
            catalogs = self.page.inner_html(".card__aside")
            if 'jpg&amp;v=2&amp;wpsid=9' in catalogs:
                print(f"\n{catalogs}")
            else:
                print(f"\nСсылка не распознана")


if __name__ == '__main__':
    unittest.main()
