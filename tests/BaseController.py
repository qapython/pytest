import pytest
import unittest
import requests
from models.accounts import *
from models.links import *
from models.locators import *
from playwright.sync_api import Page
from pytest_testrail.plugin import pytestrail


class LoginPage:
    def __init__(self, page):
        self.page = page
    
    def navigate(self):
        self.page.goto(home_pages)
    
    def login(self):
        self.page.goto(brokerage_auth)
        self.page.fill(selector[0], user)
        self.page.fill(selector[1], password)
        self.page.click(selector[4])
    
    """Переделать локаторы"""
    def login_phone(self):
        self.page.goto(brokerage_auth)
        self.page.click("[tabindex='-1']")
        self.page.fill(selector[6], phone)
        self.page.click("#login_phone_click")
        self.page.fill(selector[7], passphone)
        self.page.click("#login_phone_pass_click")
        self.page.click("//a[contains(.,'ипотека')]")
        self.page.click("//a[.='Работа с заявками']")
        self.page.click("//button[.='Все заявки']")
        self.page.click("//a[.='2690 (Нет брони)  - Тестовый Тест Тестович']")


class Assortement(LoginPage):
    def __init__(self, page):
        super().__init__(page)

    def login_assortement(self):
        self.page.goto(assortement)
        self.page.fill(selector[0], user)
        self.page.fill(selector[1], password)
        self.page.click(selector[4])


class MiniAnketa(LoginPage):
    def __init__(self, page):
        super().__init__(page)
    
    def send_requests(self):
        self.page.goto(mortgage)
        self.page.click(buttons[0])


class RegionPage:
    def __init__(self, page):
        self.page = page

    def menu(self):
        self.page.goto(home_pages)

    def change(self):
        self.page.click(change)
        self.page.click(mbtn)
        self.page.click(change)
        self.page.click(mspb)
        self.page.click(change)
        self.page.click(mvlad)
        self.page.click(change)
        self.page.click(mvladimir)
  

class CatalogSettings(LoginPage):
    def __init__(self, page):
        self.page = page

    def auth(self):
        self.page.goto(brokerage_auth)
        self.page.fill(selector[0], user)
        self.page.fill(selector[1], password)
        self.page.click(selector[4])


class RoomChange:
    def __init__(self, page):
        self.page = page

    def pageGoto(self):
        self.page.goto(allrealty)