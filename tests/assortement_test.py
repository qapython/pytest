from BaseController import *


def test_assortement_subway_check(page, browser_context_args):
    assort_page = Assortement(page)
    assort_page.login_assortement()
    # Click [//input[@id='name']"
    page.click(assortements[0])

    # Fill [//input[@id='name']
    for sub in subwayss:
        page.fill(assortements[0], sub)
        # Press Enter
        page.press(assortements[0], "Enter")
        page.click(assortements[2])
        subways = page.inner_text(assortements[1])
        assert sub.split()[:-1] == subways.split()
        page.click(assortements[3])
        print(subways)

    return {
        **browser_context_args,
        "ignore_https_errors": True
    }
