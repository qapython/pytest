import pytest
import logging
import unittest
from models.links import *
from models.locators import *
from playwright.sync_api import Page


logging.basicConfig(level=logging.WARNING)
logger = logging.getLogger()


class StatusHouses(unittest.TestCase):
    @pytest.fixture(autouse=True)
    def setup(self, page: Page):
        self.page = page

    def test_verify_status_house(self):
        self.page.goto(allrealty)
        # Click [placeholder="Лучший ЖК"]
        self.page.click("[placeholder=\"Лучший ЖК\"]")
        # Fill [placeholder="Лучший ЖК"]
        for filters in filter_search:
            self.page.fill("[placeholder=\"Лучший ЖК\"]", filters)
            # Press Enter
            self.page.press("[placeholder=\"Лучший ЖК\"]", "Enter")
            self.page.click("text=Найти")
            card = self.page.inner_html(".card__title")
            html = self.page.inner_html(".card-timing__value")
            if 'Сдан' in html:
                print(f"\n{html} {card}")
            else:
                assert html == html


if __name__ == '__main__':
    unittest.main()
