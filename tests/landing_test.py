import pytest
import requests

from models.links import *
from models.locators import *
from BaseController import LoginPage, RegionPage, RoomChange


def test_verify_landing_dev(page, browser_context_args):
    page.goto(ecatalog)
    r = requests.get(ecatalog)
    assert r.status_code == 200
    for locator in locators_page:
        headers = page.inner_text(locator)
        print(f'\n{headers}')
    return {
        **browser_context_args,
        "ignore_https_errors": True
    }


def test_verify_landing_rc(page, browser_context_args):
    page.goto(sitepro)
    r = requests.get(sitepro)
    assert r.status_code == 200
    for locator in locators_page:
        headers = page.inner_text(locator)
        print(f'\n{headers}')
    return {
        **browser_context_args,
        "ignore_https_errors": True
    }


def test_verify_landing_prod(page, browser_context_args):
    page.goto(eprod)
    r = requests.get(eprod)
    assert r.status_code == 200
    for locator in locators_page:
        headers = page.inner_text(locator)
        print(f'\n{headers}')
    return {
        **browser_context_args,
        "ignore_https_errors": True
    }
